// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })

  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common["X-Request-Width"];
    $httpProvider.defaults.headers.post["Content-type"] = "application/x-www-form-urlencoded; charset=UTF-8";

    $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
      })

      .state('app.registrar_grupo', {
        url: '/registrar_grupo',
        views: {
          'menuContent': {
            templateUrl: 'templates/registrar_grupo.html'
          }
        }
      })

      .state('app.registrar_usuario', {
        url: '/registrar_usuario',
        views: {
          'menuContent': {
            templateUrl: 'templates/registrar_usuario.html'
          }
        }
      })

      .state('app.subir_codigo', {
        url: '/subir_codigo',
        views: {
          'menuContent': {
            templateUrl: 'templates/subir_codigo.html'
          }
        }
      })

      .state('app.acercade', {
        url: '/acercade',
        views: {
          'menuContent': {
            templateUrl: 'templates/acercade.html'
          }
        }
      })

      .state('app.ejercicios_realizados', {
        url: '/ejercicios_realizados',
        views: {
          'menuContent': {
            templateUrl: 'templates/ejercicios_realizados.html'
          }
        }
      })

      .state('app.competencias', {
        url: '/competencias',
        views: {
          'menuContent': {
            templateUrl: 'templates/competencias.html'
          }
        }
      })

      .state('app.eventos', {
        url: '/eventos',
        views: {
          'menuContent': {
            templateUrl: 'templates/eventos.html'
          }
        }
      })

      .state('app.cursos', {
        url: '/cursos',
        views: {
          'menuContent': {
            templateUrl: 'templates/cursos.html'
          }
        }
      })

      .state('app.editorvivo', {
        url: '/editorvivo',
        views: {
          'menuContent': {
            templateUrl: 'templates/editor_vivo.html'
          }
        }
      })

      .state('app.dash', {
        url: '/dash',
        views: {
          'menuContent': {
            templateUrl: 'templates/dash.html'
          }
        }
      })

      .state('app.prueba', {
        url: '/prueba',
        views: {
          'menuContent': {
            templateUrl: 'templates/prueba.html'
          }
        }
      })

      .state('app.playlists', {
        url: '/playlists',
        views: {
          'menuContent': {
            templateUrl: 'templates/playlists.html',
            controller: 'PlaylistsCtrl'
          }
        }
      })

      .state('app.single', {
        url: '/playlists/:playlistId',
        views: {
          'menuContent': {
            templateUrl: 'templates/playlist.html',
            controller: 'PlaylistsCtrl'
          }
        }
      });
  
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app /dash');
  });
