angular.module('starter.controllers', ["ngMaterial", "ngResource"])

  .controller('AppCtrl', function ($scope, $http, $ionicModal,
    $timeout, $state, $mdToast, getEjercicios) {

    /**
     * IP del servidor a donde se van a realizar las solicitudes
     * (Ruta de escucha del API REST)
     */
    var IP = 'http://200.21.7.94';
    //var IP = 'http://127.0.0.1';
    //var IP = 'http://192.168.1.39';
    
    /**
     * Puerto de escucha de la direccion IP de arriba
     */
    var port = '3000';
    
    /**
     * Validar si el usuario ha iniciado sesion
     */
    var verificar_sesion = false;
    
    /**
     * Rutas
     */
    var servidor = IP + ':' + port + '/';
    var do_Login = servidor + 'usuarios/auth/';
    
    //get
    var getData = servidor + 'get/';
    var get_CuentaPersona = servidor + 'cuenta_persona/get';
    
    //add
    var addCuenta = servidor + "cuenta/add/";
    var addGrupo = servidor + "grupo/add/";
    
    //Variable de idPersona, Se asigna cuando se inicia sesion
    $scope.idPersona = 0;
    $scope.Perfil = {};

    $scope.getData = function (modelo) {
      var List = [];
      $http.get(getData + modelo).success(function (data) {
        data.forEach(function (d) {
          List.push(d);
        }, this);
      });
      return List;
    };

    $scope.whitelist = function () {

      console.log('method called');
      $http.defaults.headers.post["Content-Type"] = "application/json";

      $http.get("http://200.21.7.94:3000").then(function (data) {
        console.log(data);
        $scope.categories = data.status;
        console.log($scope.categories);
        alert("Holaaaa, " + data);
      }, function (error) {
        console.log(error);
        alert("d' oh!" + error[0] + error[1] + "\\" + error);
      });
    }

    $scope.getData_test = function () {
      var req = {
        method: 'GET',
        url: getData + 'persona',
        headers: {
          'Content-Type': 'application/json',
        }
      };

      // Make the API call
      $http(req).success(function (resp) {
        // Handle success
        console.log("Ionic Push: Push success!" + resp);
      }).error(function (error) {
        // Handle error 
        console.log("Ionic Push: Push error...");
      });

      /**  var List = [];
       $http.get(getData + modelo).success(function (data) {
         data.forEach(function (d) {
          
         }, this);
       });
       return List;
       */
    };



    $scope.getData_CuentaPersona = function () {
      $scope.List_CuentaPersona = [];
      $http.get(get_CuentaPersona).success(function (data) {
        data.forEach(function (d) {
          $scope.List_CuentaPersona.push(d);
        }, this);
      });
    };

    $scope.getData_Competencia = function () {
      $scope.List_Competencia = $scope.getData('competencia');
    };

    $scope.getData_TipoDocumento = function () {
      $scope.List_TipoDocumento = $scope.getData('tipodocumento');
    };

    $scope.getData_Programa = function () {
      $scope.List_Programa = $scope.getData('programa');
    };

    $scope.getData_Ejercicios = function () {
      $scope.List_Ejercicios = $scope.getData('ejercicios');
    };

    $scope.getData_Cursos = function () {
      $scope.List_Cursos = $scope.getData('cursos');
    };

    $scope.getData_Competencias = function () {
      $scope.List_Competencias = $scope.getData('competencia');
    };

    $scope.boton_Registrarme = function () {
      $state.go('app.registrar_usuario');
      $timeout(function () {
        $scope.closeLogin();
      }, 1000);
    };

    $scope.registrarCuenta = function () {
      console.log("Entro al metodo de registrar cuenta.");
      var email = this.usuario.email;
      var pass = this.usuario.pass;
      var nombres = this.usuario.nombres;
      var apellidos = this.usuario.apellidos;
      var telefono = this.usuario.telefono;
      var programa = this.usuario.programa;
      var tipodoc = this.usuario.tipodoc;
      var documento = this.usuario.documento;

      if (!!email && !!pass && !!nombres
        && !!apellidos && !!telefono && !!programa
        && !!tipodoc && !!documento) {

        console.log("Validacion correcta.");

        // cuenta/add/:NUsuario/:Contrasenia/:fk_idTipoDocumento/:NDocumento/:Nombres/:Apellidos/:Telefono/:Correo/:fk_idPrograma
        var s = '/';
        var inf = email + s + pass + s + tipodoc + s + documento + s + nombres + s + apellidos + s + telefono + s + email + s + programa;
        $http({
          method: 'POST',
          url: addCuenta + inf
        }).then(function successCallback(response) {
          console.log("Registro llevado con exito." + response);
          $scope.notificacionSimple("Registro llevado con exito.\nInicie con Email: " + email + "   Clave:" + pass);
          $scope.modal.show();
        }, function errorCallback(response) {
          console.log("ERROR: " + response);
          $scope.notificacionSimple("Registro llevado con exito. Email: " + email + "   Clave:" + pass);
        });
      } else {
        console.log(email + pass + nombres + apellidos + telefono + programa + tipodoc + documento);
        $scope.notificacionSimple("Toast: Por favor complete todos los campos.");
        console.log("Toast: Por favor complete todos los campos.");
      }
    };


    $scope.registrarGrupo = function () {
      console.log("Entro al metodo de registrar grupo.");

      var nombregrupo = this.grupo.nombregrupo;
      var nusuario = this.grupo.nusuario;
      var contrasenia = this.grupo.contrasenia;
      var idCuenta1 = this.grupo.idCuenta1;
      var idCuenta2 = this.grupo.idCuenta2;
      var idCuenta3 = this.grupo.idCuenta3;
      var competencia = this.grupo.competencia;

      if (!!nombregrupo && !!nusuario && !!contrasenia
        && !!idCuenta1 && !!idCuenta2 && !!idCuenta3
        && !!competencia) {

        console.log("Validacion correcta.");

        ///grupo/add/:NombreGrupo/:UserGrupo/:Contrasenia/:FK_idPersona1/:FK_idPersona2/:FK_idPersona3/:FK_idCompetencia
        var s = '/';
        var inf = nombregrupo + s + nusuario + s + contrasenia + s + idCuenta1 + s + idCuenta2 + s + idCuenta3 + s + competencia;
        $http({
          method: 'POST',
          url: addGrupo + inf,
          transformRequest: false,
          headers: { 'Content-Type': undefined }
        }).then(function successCallback(response) {
          console.log("Registro llevado con exito." + response);
          $scope.notificacionSimple("Registro llevado con exito");
        }, function errorCallback(response) {
          console.log("ERROR: " + response);
          $scope.notificacionSimple("Registro llevado con error.");
        });
      } else {
        $scope.notificacionSimple("Toast: Por favor complete todos los campos.");
        console.log("Toast: Por favor complete todos los campos.");
      }
    };

    
    
    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function (modal) {
      $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function () {
      $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function () {
      $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function () {
      console.log('Doing login', $scope.loginData);

      var dir = do_Login + $scope.loginData.username + '/' + $scope.loginData.password;

      console.log("Direccion" + dir);
      $http.get(dir)
        .success(function (data, status, headers, config) {
          console.log("**** SUCCESS ****");
          console.log(status);
          console.log("infor: " + data);
          //alert("La consulta se realizo. Data: " + data);
          if (data) {
            console.log("Se trajo esto: " + data);
            if (data == "error") {
              console.log("Se obtuvo un error.");
              $scope.notificacionOK("Nombre de usuario y/o contraseña incorrectos.");
            } else if (!!data[0]) {
              console.log("Inicio sesion con exito");
              alert("Inicio sesion correctamente");
              $state.go('app.dash');            
              //Variable de control de logueo
              $scope.logueado = true;
              $scope.idPersona = data[0].FK_idPersona;
              $scope.Perfil = data[0];
              console.log("ID PERSONA: " + $scope.idPersona);
              $scope.notificacionSimple("Inicio sesion con exito.\nBienvenido " + $scope.Perfil.Nombres);
              $timeout(function () {
                $scope.closeLogin();
              }, 1000);
            }
          } else {
            console.error("No se pudo iniciar sesion, tal vez los credenciales son incorrectos o no estas registrado");
            alert("No se pudo iniciar sesion");
            $scope.logueado = false;
            $scope.login();
          }
        })
        .error(function (data, status, headers, config) {
          console.log("**** ERROR ****");
          alert("Error al iniciar sesion" + data + status + headers + config);
          console.log(status);
          $scope.logueado = false;
          $scope.notificacionOK("Error: " + data + status + config[0] + config[1]);
        });
      
      // Simulate a login delay. Remove this and replace with your login
      // code if using a login system
      /* $timeout(function () {
         $scope.closeLogin();
       }, 1000);*/
    };

    $scope.validarLogin = function () {
      if (!$scope.logueado && verificar_sesion) {
        $scope.login();
      }
    };

  
    /**
     *                                  Notificaciones Toast
     */
    //Mostrar Notificacion toast
    var last = {
      bottom: false,
      top: true,
      left: false,
      right: true
    };
    $scope.toastPosition = angular.extend({}, last);
    $scope.getToastPosition = function () {
      sanitizePosition();
      return Object.keys($scope.toastPosition)
        .filter(function (pos) { return $scope.toastPosition[pos]; })
        .join(' ');
    };
    function sanitizePosition() {
      var current = $scope.toastPosition;
      if (current.bottom && last.top) current.top = false;
      if (current.top && last.bottom) current.bottom = false;
      if (current.right && last.left) current.left = false;
      if (current.left && last.right) current.right = false;
      last = angular.extend({}, current);
    }
    $scope.showCustomToast = function () {
      $mdToast.show({
        controller: 'ToastCtrl',
        templateUrl: 'toast.html',
        parent: $document[0].querySelector('#toastBounds'),
        hideDelay: 6000,
        position: $scope.getToastPosition()
      });
    };
    $scope.showSimpleToast = function () {
      $mdToast.show(
        $mdToast.simple()
          .content('Simple Toast!')
          .position($scope.getToastPosition())
          .hideDelay(3000)
        );
    };
    $scope.notificacionSimple = function (contenido) {
      $mdToast.show(
        $mdToast.simple()
          .content(contenido)
          .position($scope.getToastPosition())
          .hideDelay(3000)
        );
    };
    $scope.showActionToast = function () {
      var toast = $mdToast.simple()
        .content('Action Toast!')
        .action('OK')
        .highlightAction(false)
        .position($scope.getToastPosition());
      $mdToast.show(toast).then(function (response) {
        if (response == 'ok') {
          alert('You clicked \'OK\'.');
        }
      });
    };
    $scope.notificacionOK = function (contenido) {
      var toast = $mdToast.simple()
        .content(contenido)
        .action('OK')
        .highlightAction(false)
        .position($scope.getToastPosition());
      $mdToast.show(toast).then(function (response) {
        if (response == 'ok') {
          alert('You clicked \'OK\'.');
        }
      });
    };
    $scope.closeToast = function () {
      $mdToast.hide();
    };
  })//Fin de controller('AppCtrl'.....

  .controller('PlaylistsCtrl', function ($scope) {
    $scope.playlists = [
      { title: 'Reggae', info: 'Genero reggae', id: 1 },
      { title: 'Chill', info: 'Genero chill', id: 2 },
      { title: 'Dubstep', info: 'Genero dubstep', id: 3 },
      { title: 'Indie', info: 'Genero indie', id: 4 },
      { title: 'Rap', info: 'Genero reggae', id: 5 },
      { title: 'Cowbell', info: 'Genero Cowbell', id: 6 },
      { title: 'Rock', info: 'Genero rock', id: 7 }
    ];
  })

  .controller('PlaylistCtrl', function ($scope, $stateParams) {

  })


  .factory("getEjercicios", function ($resource) {
    //var url = "http://200.21.7.94:3000/get/ejercicios";
    var url = "http://192.168.1.39:3000/get/ejercicios";
    return $resource(url, {}, {
      update: {
        method: "GET",
        params: {}
      }
    });
  })
  
  